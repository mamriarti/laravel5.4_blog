<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;

class PostsController extends Controller
{
    public function index(){
        $posts = Post::latest()->get();
    	return view('posts.index', compact('posts'));
    }


    public function create(){
    	return view('posts.create');
    }

    public function store(){
    	// dd(request()->all());
    	// $post = new Post;
    	// $post->title = request('title');
    	// $post->body = request('body');
    	// $post->save();
    

    	// Post::create([
    	// 	'title' => request('title'),
    	// 	'body' => request('body')

    	// 	]);
    	$this->validate(request(),[
    		'title' => 'required',
    		'body'  => 'required'
    		]);

    	Post::create([
            'title' => request('title'),
            'body'  => request('body'),
            'user_id' => 1,
        ]);

    	return redirect('/');

    }
    public function show(Post $post){
    	// $post = Post::find($id);
    	return view('posts.show', compact('post'));

    }
}
